using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager1 : MonoBehaviour
{
    public Transform BlockContain;
    public static GameManager1 Current;
    public PlayerFieldGrid playerField;
    public TetrisBlock[] TetrisRefabList;
    public InputManager InputManager;
    private void Awake()
    {
        if(Current == null)
        {
            Current = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        playerField.GenerateGrid();
    }

    public void StartGame()
    {
        SpawnBlock();
    }

    public bool CheckCanMove(Vector3 pos, MoveDirection direction)
    {
        return playerField.CheckCanMove(pos, direction);
    }

    private void SpawnBlock()
    {
        var r = Random.Range(0, TetrisRefabList.Length);
        var block = Instantiate(TetrisRefabList[r], BlockContain);
        block.transform.position = new Vector3Int((int)(playerField.X / 2), playerField.Y, (int)(playerField.Z / 2));
        block.Init();
        InputManager.RegistMoveAction(block.OnMovement);
    }
}
