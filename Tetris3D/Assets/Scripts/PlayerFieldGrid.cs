using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFieldGrid : MonoBehaviour
{
    public static PlayerFieldGrid Current;
    public Transform BottomPlane;
    public Transform E;
    public Transform W;
    public Transform S;
    public Transform N;
    
    public int X;
    public int Y;
    public int Z;
    private Stack<int>[] listData;
    private void Awake()
    {
        Current = this;
    }

    public void GenerateGrid()
    {
        CreateGridData();
        OnDrawGrid(X, Z, ref BottomPlane);
        OnDrawGrid(Z, Y, ref E);
        OnDrawGrid(Z, Y, ref W);
        OnDrawGrid(X, Y, ref S);
        OnDrawGrid(X, Y, ref N);
        SetPosition();
        
    }

    private void CreateGridData()
    {
        int total = X * Z;
        listData = new Stack<int>[X * Y];
        int count = 0;
       
        while (count < total)
        {
            listData[count] = new Stack<int>();
            listData[count].Push(0);
            count++;
        }
    }

    public int GetHeight(float x, float z)
    {
        var posX = (int)x;
        var posY = (int)z;
        var dt = GetData(posX, posY);
        if(dt != null)
        {
            return dt.Peek();
        }
        return 0; 
    }

    public Stack<int> GetData(int x, int z)
    {
        var key = z * Z + x;
        if(key >= 0 && key <= X * Z)
        {
            return listData[key];
        }
        return null;
    }

    private void OnDrawGrid(int _width, int _height, ref Transform plane)
    {
        // Set Size Plane
        Vector3 size = new Vector3(_width * 1f / 10, 1, _height * 1f / 10);
        plane.localScale = size;

        //Set Material
        plane.GetComponent<MeshRenderer>().material.mainTextureScale = new Vector2(_width, _height);
    }

    private void SetPosition()
    {
        BottomPlane.localPosition = new Vector3((float)X / 2, 0, (float)Z / 2);
        E.localPosition = new Vector3(X, (float)Y / 2, (float)Z / 2);
        W.localPosition = new Vector3(0, (float)Y / 2, (float)Z / 2);
        S.localPosition = new Vector3((float)X / 2, (float)Y / 2, 0);
        N.localPosition = new Vector3((float)X / 2, (float)Y / 2, Z);
    }

    public bool CheckCanMove(Vector3 pos, MoveDirection dir)
    {
        Vector3Int checkPos = Round(pos);
        switch(dir)
        {
            case MoveDirection.Left:
                if(checkPos.x > 0)
                {
                    return true;
                }
                break;
            case MoveDirection.Right:
                if (checkPos.x < X)
                {
                    return true;
                }
                break;
            case MoveDirection.Forward:
                if (checkPos.z > 0)
                {
                    return true;
                }
                break;
            case MoveDirection.Back:
                if (checkPos.x < Z)
                {
                    return true;
                }
                break;
            case MoveDirection.Down:
                if (checkPos.y > 0)
                {
                    return true;
                }
                break;
        }
        return false;
    }

    private Vector3Int Round(Vector3 pos)
    {
        return new Vector3Int(Mathf.FloorToInt(pos.x),
            Mathf.FloorToInt(pos.y),
            Mathf.FloorToInt(pos.z));
    }
    public void UpdateGrid(int x, int y, int z)
    {
        var dt = GetData(x, z);
        if (dt != null)
        {
            dt.Push(y);
        }
    }
}
