using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TetrisGhost : MonoBehaviour
{
    public class TetrisGhostData
    {
        public int X;
        public int Z;
        public int counter;
    }
    private List<TetrisGhostData> listCubeData;
    private void GetListCubeData()
    {
        if(listCubeData is null)
        {
            listCubeData = new();
        }
        else
        {
            listCubeData.Clear();
        }

        foreach (Transform cube in transform)
        {
            var pos = cube.position;
            var a = listCubeData.FirstOrDefault((v) => v.X.Equals((int)pos.x) && v.Z.Equals((int)pos.z));
            if(a is not null)
            {
                a.counter++;
            }
            else
            {
                listCubeData.Add(new TetrisGhostData()
                {
                    X = (int)pos.x,
                    Z = (int)pos.z,
                    counter = 1,
                });
            }
        }
    }
    private void CheckHeight()
    {
        GetListCubeData();
        int max = 0;
        foreach (var cube in listCubeData)
        {
           var data = PlayerFieldGrid.Current.GetData(cube.X, cube.Z);
        }
    }
}
