using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Helper 
{
    public static int LastOfArray(object[] array)
    {
        for (int i = array.Length - 1; i >= 0; i--)
        {
            if(array[i] is not null)
            {
                return i;
            }
        }
        return -1;
    }
}
