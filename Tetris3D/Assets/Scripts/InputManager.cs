using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveDirection
{
    Left,
    Right,
    Forward,
    Back,
    Down,
}
public class InputManager : MonoBehaviour
{
    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKeyUp(KeyCode.A))
        {
            DispatchAction(MoveDirection.Left);
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            DispatchAction(MoveDirection.Right);
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            DispatchAction(MoveDirection.Back);
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            DispatchAction(MoveDirection.Forward);
        }
        else if(Input.GetKeyUp(KeyCode.Space))
        {
            DispatchAction(MoveDirection.Down);
        }
    }

    private Action<MoveDirection> moveAction;
    public void RegistMoveAction(Action<MoveDirection> act)
    {
        if(act != null)
        {
            moveAction = act;
        }
    }

    public void UnRegistMoveAction(Action<MoveDirection> act)
    {
        if(moveAction != null)
        {
            moveAction -= act;
        }
    }
    public void DispatchAction(MoveDirection direction)
    {
        moveAction?.Invoke(direction);
    }
}
