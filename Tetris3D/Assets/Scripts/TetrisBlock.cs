using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TetrisBlock : MonoBehaviour
{
    private float timeCount;
    private float timeFall = 1;
    public bool init;

    public void Init()
    {
        GetLowerCube();
        init = true;
    }

    private void FixedUpdate()
    {
        if (init == false) return;

        timeCount += Time.fixedDeltaTime;
       
        if (CheckValidFall())
        {
            if (timeCount >= timeFall)
            {
                timeCount = 0;
                transform.position += Vector3.down;
            }
        }
        else
        {
            enabled = false;
        }
    }

    private bool CheckValidFall()
    {
        foreach (var cube in lowerCubeList)
        {
            var height = cube.position.y - PlayerFieldGrid.Current.GetHeight(cube.position.x, cube.position.z);
            if(height <= 0)
            {
                return false;
            }
        }
        return true;
    }
    private List<Transform> lowerCubeList;
    private void GetLowerCube()
    {
        if(lowerCubeList == null)
        {
            lowerCubeList = new();
        }
        else
        {
            lowerCubeList.Clear();
        }
        
        foreach (Transform child in transform)
        {
            var cube = lowerCubeList.FirstOrDefault((v) => v.position.x == child.position.x && v.position.z == child.position.z);
            if(cube != null)
            {
                if(cube.transform.position.y > child.position.y)
                {
                    lowerCubeList.Add(child);
                }
            }
            else
            {
                lowerCubeList.Add(child);
            }
        }
    }

    public void OnMovement(MoveDirection direction)
    {
        if(CheckMove(direction))
        {
            switch(direction)
            {
                case MoveDirection.Left:
                    transform.position += Vector3.left;
                    break;
                case MoveDirection.Right:
                    transform.position += Vector3.right;
                    break;
                case MoveDirection.Forward:
                    transform.position += Vector3.forward;
                    break;
                case MoveDirection.Back:
                    transform.position += Vector3.back;
                    break;
                case MoveDirection.Down:
                    transform.position += Vector3.down;
                    break;
            }
        }
    }

    private bool CheckMove(MoveDirection direction)
    {
        foreach (Transform child in transform)
        {
            if(!GameManager1.Current.CheckCanMove(child.position,direction))
            {
                return false;
            }
        }
        return true;
    }
}
